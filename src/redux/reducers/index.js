import { combineReducers } from "redux";
import hand from './hand'
import appData from "./appData";

const rootReducer = combineReducers({
    hand,
    appData
});

export default rootReducer;