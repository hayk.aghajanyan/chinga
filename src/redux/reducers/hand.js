const initialState = {
    activeHand: null,
    handType: null,
    handHistory: []
}

const hand = (state = initialState, action) => {
    const {type, payload} = action
    switch (type) {
        case 'SET_HAND':
            return {
                ...state,
                activeHand: payload
            }
        case 'SET_BET':
            return {
                ...state,
                handType: payload
            }
        case 'SET_HAND_HISTORY':
            if(state.handHistory.length === 4) {
                state.handHistory.shift()
            }
            return {
                ...state,
                handHistory: [
                    ...state.handHistory,
                    payload
                ]
            }
        case 'RESET_HAND':
            return initialState
        case 'RENEW_HAND':
            return {
                ...initialState,
                handHistory: state.handHistory
            }
        default:
            return state
    }
}

export default hand