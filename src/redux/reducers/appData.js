const initialState = {
    autoGame: false,
    gameStarted: false,
    comparisonStarted: false,
    resultScreen: false,
    winner: undefined,
    drawId: null,
    playerId: null,
    bet: null,
    avatarId: null,
}

const appData = (state = initialState, action) => {
    const {type, payload, drawId, playerId, bet, avatarId} = action
    switch (type) {
        case 'SET_AUTOGAME_MODE':
            return {
                ...state,
                autoGame: payload
            }
        case 'SET_GAME_STARTED':
            return {
                ...state,
                gameStarted: true,
                drawId,
                playerId,
                bet,
                avatarId,
            }
        case 'RESET_GAME':
            return initialState
        case 'START_COMPARISON':
            return {
                ...state,
                comparisonStarted: true
            }
        case 'SET_RESULT_SCREEN':
            return {
                ...state,
                comparisonStarted: false,
                resultScreen: true,
                winner: payload
            }
        case 'RENEW_GAME':
            return {
                ...state,
                resultScreen: false
            }
        default:
            return state
    }
}

export default appData