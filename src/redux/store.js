import { createStore, compose, applyMiddleware } from "redux";
import rootReducer from "./reducers";
import thunk from "redux-thunk";
import {enableBatching} from "redux-batched-actions";


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(enableBatching(rootReducer), composeEnhancers(applyMiddleware(thunk)));