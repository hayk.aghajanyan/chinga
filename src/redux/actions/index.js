export const setHand = payload => ({
    type: 'SET_HAND',
    payload
})

export const setAutoGameMode = payload => ({
    type: 'SET_AUTOGAME_MODE',
    payload
})

export const setHandHistory = payload => ({
    type: 'SET_HAND_HISTORY',
    payload
})

export const setGameStarted = (drawId, playerId, bet, avatarId) => ({
    type: 'SET_GAME_STARTED',
    drawId,
    playerId,
    bet,
    avatarId,
})

export const resetGame = () => ({
    type: 'RESET_GAME',
})

export const resetHand = () => ({
    type: 'RESET_HAND',
})

export const setBet = payload => ({
    type: 'SET_BET',
    payload
})

export const setResultScreen = payload => ({
    type: 'SET_RESULT_SCREEN',
    payload
})

export const renewGame = () => ({
    type: 'RENEW_GAME',
})

export const startComparison = () => ({
    type: 'START_COMPARISON'
})

export const renewHand = () => ({
    type: 'RENEW_HAND'
})