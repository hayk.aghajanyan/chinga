import React from 'react'
import classNames from 'classnames'
import {connect} from "react-redux";
import {setAutoGameMode, setHand} from "../../redux/actions";
import {batchActions} from "redux-batched-actions";

const Hands = ({activeHand, dispatch}) => {
    const handHandler = bet => {
        dispatch(batchActions([setHand(bet), setAutoGameMode(false)]))
    }

    return (
        <div className='hands'>
            <div onClick={() => handHandler(1)} className={classNames('hand', {
                active: activeHand === 1
            })}>1</div>
            <div onClick={() => handHandler(2)} className={classNames('hand', {
                active: activeHand === 2
            })}>2</div>
            <div onClick={() => handHandler(3)} className={classNames('hand', {
                active: activeHand === 3
            })}>3</div>
        </div>
    )
}

const mapStateToProps = ({hand}) => {
    return {
        activeHand: hand.activeHand
    }
}

export default connect(mapStateToProps)(Hands)