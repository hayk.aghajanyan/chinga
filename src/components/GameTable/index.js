import React from 'react'
import Controls from "../Controls";
import Hands from "../Hands";
import PlayerInfo from "../PlayerInfo";
import GameHeader from "../GameHeader";
import {useSelector} from "react-redux";

const GameTable = () => {
    const {playerId, bet, avatarId} = useSelector(({appData}) => appData)
    return (
        <>
            <Hands />
            <Controls />
            <PlayerInfo id={playerId} player={false} avatar={avatarId} />
            <GameHeader bet={bet} />
            <PlayerInfo id={'1111'} player/>
        </>
    )
}

export default GameTable