import React, {useEffect} from 'react'
import {useDispatch} from "react-redux";
import {renewGame, renewHand} from "../../redux/actions";
import {batchActions} from "redux-batched-actions";

const WinnerScreen = ({winner}) => {
    const dispatch = useDispatch()
    useEffect(() => {
        setTimeout(() => {
            dispatch(batchActions(([renewGame(), renewHand()])))
        }, 3000)
    }, [])
    return (
        <>
            {winner}
        </>
    )
}

export default WinnerScreen