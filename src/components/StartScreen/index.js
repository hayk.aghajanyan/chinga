import React, {useState, useEffect} from "react";
import '../../App.css';
import GameTable from "../../components/GameTable";
import {connect, useDispatch, useSelector} from "react-redux";
import {setGameStarted} from "../../redux/actions";
import {waitingGameCardsData} from '../../fakeData'
import GameAnimation from "../GameAnimation";
import WinnerScreen from "../WinnerScreen";

const screens = [
    'GAME_TABLE',
    'GAME_ANIMATION',
    'WINNER_SCREEN',
]

const startScreenSelector = ({appData, hand}) => {
    const {gameStarted, drawId, comparisonStarted, resultScreen, winner} = appData
    return {
        isOpen: gameStarted,
        drawId,
        currentHand: hand.handType,
        comparisonStarted,
        resultScreen,
        decision: winner
    }
}

const StartScreen = () => {
    const dispatch = useDispatch()
    const {isOpen, drawId, currentHand, comparisonStarted, resultScreen, decision} = useSelector(startScreenSelector)
    const [currentScreen, setCurrentScreen] = useState(screens[0])
    const changeScreen = scr => setCurrentScreen(screens[scr])
    const currentFakeOpp = waitingGameCardsData[Math.floor(Math.random() * 7)]
    const openGame = fakeOpp => dispatch(setGameStarted(fakeOpp.drawId, fakeOpp.playerId, fakeOpp.bet, fakeOpp.avatarId))

    useEffect(() => {
        !comparisonStarted && !resultScreen && changeScreen(0)
        comparisonStarted && changeScreen(1)
        resultScreen && changeScreen(2)
    }, [comparisonStarted, resultScreen])

    return (
        <div className="App">
            {!isOpen && <button onClick={() => openGame(currentFakeOpp)}>Start</button>}
            {isOpen && (
                <div className='gameTable'>
                    {currentScreen === screens[0] && <GameTable drawId={drawId}/>}
                    {currentScreen === screens[1] && <GameAnimation playerHand={currentHand}
                                                                    oppHand={currentFakeOpp.oppMoves[Math.floor(Math.random() * 5)]}/>}
                    {currentScreen === screens[2] && <WinnerScreen winner={decision}/>}
                </div>
            )}
        </div>
    );
}

export default StartScreen





