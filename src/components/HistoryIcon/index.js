import React from 'react'

const HistoryIcon = props => {
    return (
        <div className='historyIcon'>
            {props.hand}
        </div>
    )
}

export default HistoryIcon