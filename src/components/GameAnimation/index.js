import React, {useState, useEffect} from 'react'
import {winDecision} from "../../helpers/winDesision";
import {setResultScreen} from "../../redux/actions";
import {useDispatch, useSelector} from "react-redux";
import Controls from "../Controls";

const GameAnimation = ({playerHand, oppHand}) => {
    // here we should fetch
    const dispatch = useDispatch()
    const bet = useSelector(({appData}) => appData.bet)
    useEffect(() => {
        compare()
    }, [])

    const [pl, setPl] = useState(null),
        [opp, setOpp] = useState(null)

    const compare = () => {
        let player = new Promise(resolve => setTimeout(() => resolve(playerHand), 4000))
        let opponent = new Promise(resolve => setTimeout(() => resolve(oppHand), 1000))
        Promise.all([player,opponent]).then(data => {
            setPl(data[0])
            setOpp(data[1])
        })
    }

    const decision = winDecision(pl, opp)
    if(decision !== 'Something went wrong') {
        setTimeout(() => {
            dispatch(setResultScreen(decision))
        }, 1000)
    }

    return (
        <>
            Gaming animation "O-LO-LO"
            <br/>
            - - - - - - - - - - - - -
            <br/>
            {opp} | {pl}
            <br/>
            ${bet*2*0.95}
            <br/>
            {decision}
            <Controls disabled/>
        </>
    )
}

export default GameAnimation