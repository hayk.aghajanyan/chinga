import React, {useEffect} from "react";
import {resetGame, startComparison} from "../../redux/actions";
import {connect} from "react-redux";
import {useTimer} from "../../helpers/useTimer";
import {batchActions} from "redux-batched-actions";

const GameHeader = ({bet, dispatch, hand}) => {
    const [time, start, stop] = useTimer(5, 1000, true)
    useEffect(() => {
        start()
        return () => {
            stop()
        }
    }, [])
    useEffect(() => {
        if(time === 0) {
            dispatch(batchActions(([startComparison(), !hand && dispatch(resetGame())])))
        }
    }, [time])

    return (
        <div className='gameHeader'>
            <div className='gameHeader_info'>
                <div style={{display: "flex", justifyContent: 'space-evenly'}}>
                    <div>${bet}</div>
                    <div>00:{time > 9? time : '0' + time}</div>
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = ({appData, hand}) => {
    return {
        hand: hand.handType,
        isCompare: appData.comparisonStarted
    }
}

export default connect(mapStateToProps)(GameHeader)