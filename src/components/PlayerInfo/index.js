import React from 'react'
import classNames from 'classnames'
import History from "../History";
import img from '../../assets/img/img.jpg'
import {useSelector} from "react-redux";

const PlayerInfo = ({player, id}) => {
    const history = useSelector(({hand}) => hand.handHistory)

    return (
        <div className='player_info_wrapper'>
            <div className={classNames('topInfo', {
                player: player,
                opponent: !player
            })}>
                <img className='icon' src={img} alt="#"/>
                <div className={'topInfo_opponent'}>
                    <span>{player? 'You' : 'Opponent'}</span>
                    <span>ID {id}</span>
                </div>
            </div>
            <History player={player} hands={player? history : [2,2,3,3]}/>
        </div>
    )
}

export default PlayerInfo