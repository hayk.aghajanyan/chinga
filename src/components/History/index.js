import React from 'react'
import HistoryIcon from "../HistoryIcon";
import classNames from 'classnames'

const History = ({player, hands}) => {
    return (
        <div className={classNames('history', {
            left: !player,
            right: player
        })}>
            {
                hands.map(i => {
                    return <HistoryIcon key={new Date() + Math.random()} hand={i}/>
                })
            }
        </div>
    )
}

export default History