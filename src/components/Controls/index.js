import React, {useState, useEffect} from 'react'
import classNames from 'classnames'
import {connect} from "react-redux";
import {
    resetGame,
    resetHand,
    setAutoGameMode,
    setBet,
    setHand,
    setHandHistory,
} from "../../redux/actions";
import {batchActions} from "redux-batched-actions";

const Controls = ({dispatch, autoGameMode, hand, bet, disabled}) => {
    const [betStatus, setBetStatus] = useState('Place bet')
    const makeBet = bet => {
        dispatch(batchActions([setHandHistory(bet), setBet(bet)]))
        console.log(bet)
        setBetStatus('Bet accepted!')
    }

    const autoGameToggle = () => {
        dispatch(batchActions([setAutoGameMode(!autoGameMode), setHand(null)]))
    }

    const closeGame = () => {
        dispatch(batchActions([resetGame(), resetHand()]))
    }

    useEffect(() => {
        if(autoGameMode && !disabled) {
            makeBet(Math.floor(Math.random()*3+1))
        }
    }, [autoGameMode])

    return (
        <div className='controls'>
            <div className='wrapper'>
                <button disabled={disabled} onClick={closeGame} className='button'>x</button>
                <button disabled={disabled} onClick={autoGameToggle} className={classNames('button', {
                    active: autoGameMode && !hand
                })}>15 Autogame {autoGameMode? !hand?'ON' : 'OFF': 'OFF'}</button>
                <div className='betPlace'>
                    <button
                        disabled={hand == null || betStatus === 'Bet accepted!' || disabled}
                        onClick={() => makeBet(hand)}
                        className={classNames('button', {
                            active: !!hand
                        })}
                    >{betStatus}</button>
                    <div className='handPlace'>{autoGameMode? bet : hand}</div>
                </div>

            </div>
        </div>
    )
}

const mapStateToProps = ({appData, hand}) => {
    return {
        autoGameMode: appData.autoGame,
        hand: hand.activeHand,
        bet: hand.handType
    }
}

export default connect(mapStateToProps)(Controls)