import {useState, useEffect, useRef, useCallback} from 'react'

export function useTimer(initialValue, ms, isReversed) {
    const [count, setCount] = useState(initialValue);
    const intervalRef = useRef(0);

    const start = useCallback(() => {
        if (intervalRef.current !== 0) {
            return;
        }

        if (isReversed) {
            intervalRef.current = setInterval(() => {
                setCount((prev) => prev - 1);
            }, ms);
        } else {
            intervalRef.current = setInterval(() => {
                setCount((prev) => prev + 1);
            }, ms);
        }
    }, [isReversed, ms]);

    useEffect(() => {
        if (isReversed) {
            if (count === 0) {
                stop();
            }
        }
    });

    const stop = useCallback(() => {
        if (intervalRef.current === 0) {
            return;
        }
        clearInterval(intervalRef.current);
        intervalRef.current = 0;
    }, []);

    const reset = useCallback(() => {
        setCount(initialValue);
    }, [initialValue]);

    return [count, start, stop, reset];
}