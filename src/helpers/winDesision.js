export const winDecision = (player, opp) => {
    switch (player) {
        case 1:
            if(opp === 1) {
                return 'Draw!'
            } else if (opp === 2) {
                return 'You won!'
            } else {
                return 'You lost!'
            }
        case 2:
            if(opp === 1) {
                return 'You lost!'
            } else if (opp === 2) {
                return 'Draw!'
            } else {
                return 'You won!'
            }
        case 3:
            if(opp === 1) {
                return 'You won!'
            } else if (opp === 2) {
                return 'You lost!'
            } else {
                return 'Draw!'
            }
        default:
            return 'Something went wrong'
    }
}